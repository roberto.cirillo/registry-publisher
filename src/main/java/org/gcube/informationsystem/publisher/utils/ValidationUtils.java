package org.gcube.informationsystem.publisher.utils;

import static org.gcube.resources.discovery.icclient.ICFactory.client;
import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.HostingNode;
import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.ResourceMediator;
import org.gcube.common.resources.gcore.ScopeGroup;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.common.scope.impl.ScopeBean.Type;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.client.queries.impl.XQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ValidationUtils {
	
	private static final Logger log = LoggerFactory.getLogger(ValidationUtils.class);
	
	public static void valid(String name, Object object){
		if (object==null)
	          throw new IllegalArgumentException(name+" is null");
	}
	
	public  static < R extends Resource > boolean isPresent(R resource, String currentScope){
		ScopeGroup<String> scopes=resource.scopes();
		boolean founded= false;
		for(Iterator<String> it=scopes.iterator(); it.hasNext();){
			String scope=it.next();
			if(scope.equals(currentScope))
				founded=true;
		}
		if(founded)
			throw new IllegalArgumentException(" scope "+currentScope+" is already present in resource");
		return false;
	}
	
	
	/**
	 * If scope is a VRE scope and the VO and INFRA scopes are not present in the resource, this method add these scopes to the resource
	 * @param resource the resource
	 * @param scope a scope
	 */
	public static <T extends Resource> void addEnclosingScopesOnResource(T resource, String scope){
		if(new ScopeBean(scope).is(Type.VRE)){
			String voScope=new ScopeBean(scope).enclosingScope().toString();
			String infraScope=new ScopeBean(voScope).enclosingScope().toString();
// The scope collection is a set, I can add scope without checking		
			log.debug("adding "+voScope+" to the resource "+resource.id());
			ResourceMediator.setScope(resource, voScope);
			log.debug("adding "+infraScope+" to the resource "+resource.id());
			ResourceMediator.setScope(resource, infraScope);
		}else if(new ScopeBean(scope).is(Type.VO)){
			String infraScope=new ScopeBean(scope).enclosingScope().toString();
			log.debug("adding "+infraScope+" to the resource "+resource.id());
// The scope collection is a set, I can add scope without checking
			ResourceMediator.setScope(resource, infraScope);
		}
	}
	
	public static <T extends Resource> boolean isCompatibleScopeForRemove(T resource, String scope){
		log.info("scope: "+scope+" check if update is needed inr resource: "+resource.id());
		if(resource.scopes().size() == 0)
			return true;
		if(new ScopeBean(scope).is(Type.VRE)){
			log.debug(" "+scope+" is a VRE scope");
			if(anotherBrotherVREOrVOOnResource(resource, scope)){
				log.debug("found another VRE  or VO scope on the resource: ");
				return false;
			}else return true;
		}else if(new ScopeBean(scope).is(Type.VO)){
			log.debug(" "+scope+" is a VO scope");
			if(anotherSonVREOnResource(resource, scope)){
				throw new IllegalArgumentException("the resource "+resource.id()+" have another scope defined in the same VO. The VO is  "+scope);
			}else return true;
		}else{ // is a INFRA scope
			if(anotherInfraScopeOnResource(resource, scope)){
				throw new IllegalArgumentException("the resource "+resource.id()+" have another scope defined in the same INFRA. The INFRA is  "+scope);
			}else return true;
		}
	}
	
	public static <T extends Resource> boolean anotherBrotherVREOrVOOnResource(T resource, String scope){
		if(!new ScopeBean(scope).is(Type.VRE))
			throw new IllegalArgumentException("anotherBrotherVREOrVOOnResource method: the input scope must be a VRE scope");
		String enclosedScope=new ScopeBean(scope).enclosingScope().toString();
		log.debug("VO scope "+enclosedScope);
		for(String s : resource.scopes()){
			log.debug(" check scope "+s);
			if(isChildScope(enclosedScope, s)){
				log.debug("the scope "+s+" is another VRE scope defined in the resource. Not Remove needed ");
				return true;
			}else if((enclosedScope != null) && (enclosedScope.toString().equals(s))){
				log.debug("the scope "+s+" is the father VO scope defined in the resource. Not Remove needed ");
				return true;
			}
		}
		log.debug("other brother VRE scope or VO scope not found on the resource ");
		return false;
	}

	public static <T extends Resource> boolean anotherSonVREOnResource(T resource, String scope){
		if(!new ScopeBean(scope).is(Type.VO))
			throw new IllegalArgumentException("anotherSonVREOnResource method: the input scope must be a VO scope");
		for(String s : resource.scopes()){
			if(isChildScope(scope, s)){
				log.debug("the scope "+s+" is another VO scope defined in the resource ");
				return true;
			}
		}
		return false;
	}

	public static boolean isChildScope(String fatherScope, String sonScope) {
		ScopeBean currentEnclosedScope=new ScopeBean(sonScope).enclosingScope();
		if((currentEnclosedScope != null) && (currentEnclosedScope.toString().equals(fatherScope))){
			log.debug("check scope"+fatherScope+": found another son VRE scope "+sonScope);
			return true;
		}else return false;
	}

	public static <T extends Resource> boolean anotherInfraScopeOnResource(T resource, String scope){
		if(!new ScopeBean(scope).is(Type.INFRASTRUCTURE))
			throw new IllegalArgumentException("anotherInfraScopeOnResource method: the input scope must be a INFRASTRUCTURE scope");
		String infraScopeFound=null;
		for(String s : resource.scopes()){
			while(new ScopeBean(s).enclosingScope() != null){
				s=new ScopeBean(s).enclosingScope().toString();
				
			}
			infraScopeFound=s;
			if(infraScopeFound.equals(scope)){
				log.debug("check scope"+scope+": found another scope on infra "+s);
				return true;
			}
		}
		return false;
	}

//	/**
//	 * Given a scope, returns the related VO. If it is a root-VO scope, returns null
//	 * @param currentScope
//	 * @return
//	 */
//	public static String getCurrentVO(String currentScope) {
//		ScopeBean scopeBean = new ScopeBean(currentScope);
//		String currentVO=null;
//		if(scopeBean.is(Type.VRE))
//			currentVO=scopeBean.enclosingScope().toString();
//		else if (scopeBean.is(Type.VO))
//			currentVO= currentScope;
//		else return null;
//		return currentVO;
//	}
//
//	/**
//	 * Returns all the VOs involved. If just a VRE scope is present in the resource, the enclosed VO will be returned.
//	 * if present, the root-VO is returned yet
//	 * @param resource the gCube resource
//	 * @return the VO scopes
//	 */
//	public static <T extends Resource> HashSet<String> getInternalVOScopes(T resource) {
//		HashSet<String> vosScopes = new HashSet<String>();
//		log.debug("checking and collect the internal VO scopes");
//		for(String scope: resource.scopes()){
//			log.debug("processing scope: "+scope);
//			ScopeBean scopeBean = new ScopeBean(scope);
//			if(scopeBean.is(Type.VRE))
//				vosScopes.add(scopeBean.enclosingScope().toString());
////			else if(scopeBean.is(Type.VO))
//			vosScopes.add(scope);
//		}
//		return vosScopes;
//	}
//
//	public static GenericResource getGenericResourceByID(String id, String latestVO) {
//		List<GenericResource> resources;
//		String currentScope= ScopeProvider.instance.get();
//		try{
//			ScopeProvider.instance.set(latestVO);
//			SimpleQuery query = queryFor(GenericResource.class);
//			query.addCondition("$resource/ID/text() eq '"+id+"'");
//			DiscoveryClient<GenericResource> client = clientFor(GenericResource.class);
//			resources = client.submit(query);
//		}finally{
//			ScopeProvider.instance.set(currentScope);
//		}
//		if (( resources !=null) && (!resources.isEmpty()))
//			return resources.get(0);
//		else{
//			log.info(" No resource found with id "+id+" in scope "+latestVO);
//			return null;
//		}
//	}
//	
//	public static ServiceEndpoint  getServiceEndpointByID(String id, String latestVO) {
//		List<ServiceEndpoint> resources;
//		String currentScope= ScopeProvider.instance.get();
//		try{
//			ScopeProvider.instance.set(latestVO);
//			SimpleQuery query = queryFor(ServiceEndpoint.class);
//			query.addCondition("$resource/ID/text() eq '"+id+"'");
//			DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
//			resources = client.submit(query);
//		}finally{
//			ScopeProvider.instance.set(currentScope);
//		}
//		if (( resources !=null) && (!resources.isEmpty()))
//			return resources.get(0);
//		else{
//			log.info(" No resource found with id "+id+" in scope "+latestVO);
//			return null;
//		}
//	}
//
//	public static <T extends Resource> List<String> getTimestamps(T resource) {
//		XQuery query = getSpecificXQuery(resource);
//		query.addCondition("$resource/ID/text() eq '"+resource.id()+"'");
//		query.setResult("$resource/../../../../Document/LastUpdateMs/text()");
//		DiscoveryClient<String> client = client();
//		List<String> timestamps= client.submit(query);
//		return timestamps;
//	}
//
//	public static <T extends Resource> XQuery getSpecificXQuery(T resource) {
//		XQuery query = null;
//		if(resource.type().toString().equalsIgnoreCase("RuntimeResource")){
//			query = queryFor(ServiceEndpoint.class);
//		}else if(resource.type().toString().equalsIgnoreCase("GenericResource")){
//			query = queryFor(GenericResource.class);
//		}else if(resource.type().toString().equalsIgnoreCase("RunningInstance")){
//			query = queryFor(GCoreEndpoint.class);
//		}else if(resource.type().toString().equalsIgnoreCase("GHN")){
//			query = queryFor(HostingNode.class);
//		}else{
//			throw new RuntimeException("The following resource type is not managed: "+resource);
//		}
//		return query;
//	}
//
//	
//	/**
//	 * Returns the scope list found on the more recent resource (with the same id) found at VO level
//	 * @param resource
//	 * @param vosScopes
//	 * @return
//	 */
//	public static < T extends Resource> List<String> setLatestInternalScopes( T resource, HashSet<String> vosScopes){
//		log.trace("setLatestInternalScopes method, voscopes "+vosScopes+", resource id:  "+resource.id());
//		String latestVO= getMoreRecentResourceVO(resource, vosScopes);
//		if (latestVO != null)
//			return extractInternalScopes(resource, latestVO);
//		else return null;
//	}
//
//	
//	private static <T extends Resource> List<String> extractInternalScopes(T resource, String latestVO) {
//		T extractedResource=null;
//		if(resource.type().toString().equalsIgnoreCase("RuntimeResource")){
//			extractedResource= (T)ValidationUtils.getServiceEndpointByID(resource.id(), latestVO);
//		}else if(resource.type().toString().equalsIgnoreCase("GenericResource")){
//			extractedResource = (T)ValidationUtils.getGenericResourceByID(resource.id(), latestVO);
//		}else{
//			throw new RuntimeException("The following resource type is not managed: "+resource);
//		}
//		if(extractedResource.scopes().size() > 0){
//			List<String> scopesExtracted = new ArrayList<String> (extractedResource.scopes().size());
//			for(String scope: extractedResource.scopes())
//				scopesExtracted.add(scope);
//			return scopesExtracted;
//		}
//		return null;
//	}
//
//
//	
//	/**
//	 * Returns the VO where the resource is the more recent resource found in the VOs
//	 * @param resource
//	 * @param vosScopes
//	 * @return
//	 */
//	private static < T extends Resource> String getMoreRecentResourceVO(T resource, HashSet<String> vosScopes){
//		String currentScope= ScopeProvider.instance.get();
//		long timestamp=0;
//		long latestTimestamp=0;
//		String latestVO=null;
//		try{
//		for (String voScope: vosScopes){
//			ScopeProvider.instance.set(voScope);
//			log.debug("checking scope in "+voScope);
//			List<String> timestamps = ValidationUtils.getTimestamps(resource);
//			if (!timestamps.isEmpty()){
//				timestamp=Long.parseLong(timestamps.get(0).toString());
//				log.debug("checking "+voScope+" timestamp: "+timestamp+", with the more recent timestamp: "+latestTimestamp);
//				Date date = new Date(timestamp);
//				log.debug(voScope+" timestamp conversion: "+date);
//				if ( timestamp > latestTimestamp){
//					latestTimestamp=timestamp;
//					latestVO=voScope;
//					log.debug("new timestamp is "+timestamp+ " date: "+date);
//				}
//			}
//		}
//		}finally{
//			// reset scope
//			ScopeProvider.instance.set(currentScope);
//		}
//		log.debug("the vo with latest timestamp is " +latestVO);
//		log.debug("timestamp is " +latestTimestamp);
//		return latestVO;
//	}


}
